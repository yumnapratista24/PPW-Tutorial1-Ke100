from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, my_cookie_auth
from .custom_auth import auth_logout
from .api_enterkomputer import get_drones,get_soundcards,get_opticals
import requests
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),) # set default values and casting
environ.Env.read_env() # reading .env file

# Create your tests here.
class Lab9UnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		self.client = Client()

	def test_url_lab9_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code,200)

	def test_index_function(self):
		response = resolve('/lab-9/')
		self.assertEqual(response.func, index)

#Cek login berhasil atau tidak
	def test_login_failed(self):
		#Jika username/password salah maka akan redirect ke halaman login lagi
		response = self.client.post('/lab-9/custom_auth/login/' , {'username':'bombay','password':'baksos'})
		html_response = self.client.get('/lab-9/').content.decode('utf8')
		self.assertIn("Username atau password salah",html_response)

	def test_login_success(self):
		response = self.client.post('/lab-9/custom_auth/login/' , {'username':self.username,'password':self.password})
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertIn("Anda berhasil login",html_response)

	def test_login_goes_to_index(self):
		response = self.client.post('/lab-9/custom_auth/login/' , {'username':self.username,'password':self.password})
		html_response = self.client.get('/lab-9/')
		self.assertEqual(html_response.status_code,302)

	def test_goes_to_profile_but_not_logged_in(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code,302)


#Cek logout berhasil atau tidak
	def test_logout_success(self):
		#login dulu
		response = self.client.post('/lab-9/custom_auth/login/' , {'username':self.username,'password':self.password})
		self.assertEqual(response.status_code,302)
		#menjalankan logout
		response = self.client.get('/lab-9/custom_auth/logout/')
		html_response = self.client.get('/lab-9/').content.decode('utf-8')
		self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)

#Test fungsi add-delete-reset untuk drone
	def test_add_favorite_drones(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[0]['id'] + '/')
		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[1]['id'] + '/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn("Berhasil tambah drone favorite",html_response)

	def test_delete_favorite_drones(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[0]['id'] + '/')
		response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)


	def test_clear_favorite_drones(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[0]['id'] + '/')
		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[1]['id'] + '/')
		response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[2]['id'] + '/')
		
		response = self.client.post('/lab-9/clear_session_drones/')
		
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn('Berhasil reset favorite drones',html_response)

# Test fungsi login dan logout dengan cookies
	def test_login_using_cookies_success(self):
		response = self.client.post('/lab-9/cookie/auth_login/' , {'username':'utest','password':'ptest'})
		html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)

	def test_login_using_get(self):
		response = self.client.get('/lab-9/cookie/auth_login/')
		self.assertEqual(response.status_code,302)

	def test_already_login_using_cookies(self):
		response = self.client.post('/lab-9/cookie/auth_login/' , {'username':'utest','password':'ptest'})
		html_response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(html_response.status_code,302)

	def test_login_using_cookies_failed(self):
		response = self.client.post('/lab-9/cookie/auth_login/' , {'username':'bombay','password':'benta'})
		html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn("Username atau Password Salah",html_response)

	def test_logout_using_cookies(self):
		response = self.client.post('/lab-9/cookie/auth_login/' , {'username':'utest','password':'ptest'})
		#logout
		response = self.client.post('/lab-9/cookie/clear/')	
		self.assertEqual(response.status_code,302)

# Fungsi cookies lainnya
	def test_goes_to_profile_without_login(self):
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code,302)

#=============================================================================================================

#Challenge testcase

# Test fungsi umum soundcard dan optical

# Soundcard
	#Test fungsi add-delete-reset untuk drone
	def test_add_favorite_soundcard(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[0]['id'] + '/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn("Berhasil tambah soundcards favorite",html_response)

	def test_delete_favorite_soundcard(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[0]['id'] + '/')

		response = self.client.post('/lab-9/del_session_item/soundcards/' + get_soundcards().json()[0]["id"] + '/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus item soundcards dari favorite", html_response)


	def test_clear_favorite_soundcard(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[0]['id'] + '/')
		response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[1]['id'] + '/')
		response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[2]['id'] + '/')
		
		response = self.client.post('/lab-9/clear_session_item/soundcards/')
		
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn('Berhasil hapus session : favorite soundcards',html_response)

# Optical
	
	def test_add_favorite_opticals(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[0]['id'] + '/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn("Berhasil tambah opticals favorite",html_response)

	def test_delete_favorite_opticals(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[0]['id'] + '/')

		response = self.client.post('/lab-9/del_session_item/opticals/' + get_opticals().json()[0]["id"] + '/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus item opticals dari favorite", html_response)


	def test_clear_favorite_opticals(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[0]['id'] + '/')
		response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[1]['id'] + '/')
		response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[2]['id'] + '/')
		
		response = self.client.post('/lab-9/clear_session_item/opticals/')
		
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code,302)
		self.assertIn('Berhasil hapus session : favorite opticals',html_response)

	def test_masuk_profile_dengan_ngubah_cookie_manual(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302) 	

		response = self.client.cookies.load({'user_login':'upil','user_password':'terbang'})
		html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf8')

		self.assertIn('Kamu tidak punya akses :P',html_response)
		