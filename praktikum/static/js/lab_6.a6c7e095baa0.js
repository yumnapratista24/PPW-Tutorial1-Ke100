//Function to chat
var isian = "";   
var tempatChat = document.getElementById("tempat_chat");
var chat = function(event){
	if(event.keyCode === 13){
		tempatChat.value = "";
		var kotakChat = document.createElement("P");
		var isiChat = document.createTextNode(isian);
		var box = document.getElementsByClassName("msg-insert")[0];
		kotakChat.setAttribute("class","msg-send");
		kotakChat.appendChild(isiChat);
		box.appendChild(kotakChat);
		isian = "";
	}
	else{
		isian+=event.key;
		console.log(isian);
	}
}

// Calculator function
var print = document.getElementById('print');
var erase = false;
var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  } 
  else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } 
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

var theme = '[\
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},\
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},\
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},\
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},\
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},\
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},\
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},\
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},\
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},\
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},\
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}\
]';

var tema = JSON.parse(theme);
localStorage.setItem('themes',theme);
if(localStorage.selectedTheme == null){
	localStorage.setItem('selectedTheme',JSON.stringify(tema[3]));
}
$("body").css('background-color',JSON.parse(localStorage.getItem('selectedTheme')).bcgColor);
$("text-center").css('color',JSON.parse(localStorage.getItem('selectedTheme')).fontColor);

$(document).ready(function() {
    $('.my-select').select2({
    	'data':JSON.parse(localStorage.getItem('themes'))
    });    
});

$('.apply-button-class').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var selectedId = $(".my-select").val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
	localStorage.setItem('selectedTheme',JSON.stringify(tema[selectedId]));
    // [TODO] ambil object theme yang dipilih
    var ganteng = localStorage.getItem('selectedTheme');
    
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    console.log(JSON.parse(localStorage.getItem('selectedTheme')));
    $("body").css('background-color',JSON.parse(localStorage.getItem('selectedTheme')).bcgColor);
    $(".text-center").css('color',JSON.parse(localStorage.getItem('selectedTheme')).fontColor);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
})