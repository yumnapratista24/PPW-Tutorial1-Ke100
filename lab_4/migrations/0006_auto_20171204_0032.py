# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-03 17:32
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0005_auto_20171204_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 4, 0, 32, 29, 586732, tzinfo=utc)),
        ),
    ]
