from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, get_friend_list, friend_list, add_friend
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
# Create your tests here.
class Lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code,200)

	def test_get_friend_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code,200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

	def test_validate_npm(self):
 		response = self.client.post('/lab-7/validate-npm/')
 		html_response = response.content.decode('utf8')
 		self.assertEqual(response.status_code, 200)
 		self.assertJSONEqual(html_response, {'is_taken':False})

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
		response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.npm) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	def test_add_friend(self):
		url = '/lab-7/add-friend/'
		data = {'name':'Yumna','npm':'1234'}
		response = Client().post(url,data)
		self.assertEqual(response.status_code,200)

	def test_notInteger(self):
		response = Client().get('/lab-7/?page=Yumna/')
		self.assertEqual(response.status_code,200)

	def test_emptyPage(self):
		response = Client().get('/lab-7/?page=1000000000000000000000000000000000000000')
		self.assertEqual(response.status_code,200)

	def test_invalid_SSO(self):
		username = "bosi"
		password = "tompel"
		csui_helper = CSUIhelper()

		with self.assertRaises(Exception) as error:
			csui_helper.instance.get_access_token(username,password)
		self.assertIn('bosi',str(error.exception))