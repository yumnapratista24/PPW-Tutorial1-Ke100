# from django.test import TestCase
# from django.test import Client
# # Create your tests here.
# from .models import Pengguna,MovieKu
# import environ

# root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
# env = environ.Env(DEBUG=(bool, False),) # set default values and casting
# environ.Env.read_env() # reading .env file

# class Lab10UnitTest(TestCase):

# #Login test
# 	def setUp(self):
# 		self.username = env("SSO_USERNAME")
# 		self.password = env("SSO_PASSWORD")
# 		self.client = Client()

# 	def test_login_success(self):
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password}) 
# 		response = self.client.get('/lab-10/')
# 		self.assertEqual(response.status_code,302)

# 	def test_login_failed(self):
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':'bansoy','password':'botak'}) 
# 		response = self.client.get('/lab-10/')
# 		self.assertEqual(response.status_code,200)

# #Logout test
# 	def test_logout_success(self):
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password}) 
# 		response = self.client.get('/lab-10/custom_auth/logout/')
# 		self.assertEqual(response.status_code,302)

# #Test go to list
# 	def test_go_to_list(self):
# 		#test with login
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password})
# 		response = self.client.get('/lab-10/movie/list/')
# 		self.assertEqual(response.status_code,200)

# 		#test go to list with invalid SSO
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':'bandai','password':'bandaiaaa'})
# 		response = self.client.get('/lab-10/movie/list/')
# 		self.assertEqual(response.status_code,200)

# 		#Test without login
# 		response = self.client.get('/lab-10/movie/list/')
# 		self.assertEqual(response.status_code,200)		
# #Test go to detail
# 	def test_go_to_detail(self):
# 		#Logged in
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password})
# 		response = self.client.get('/lab-10/movie/detail/tt3896198/')
# 		self.assertEqual(response.status_code,200)
# 		self.client.get('/lab-10/custom_auth/logout/')

# 		#Without login
# 		response = self.client.get('/lab-10/movie/detail/tt3896198/')
# 		self.assertEqual(response.status_code,200)

# 	def test_add_to_watch_later_when_logged_in(self):
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password})
# 		response = self.client.post('/lab-10/movie/watch_later/add/tt0457648/')
# 		self.assertEqual(response.status_code,302)

# #Test go to Dashboard
# 	def test_go_to_dashboard(self):
# 		#Not logged in
# 		response = self.client.get('/lab-10/dashboard/')
# 		self.assertEqual(response.status_code,302)

# 		#Logged in
# 		response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username,'password':self.password})
# 		response = self.client.get('/lab-10/dashboard/')
# 		self.assertEqual(response.status_code,200)

# #Test add movie to watch later
# 	def test_add_to_watch_later(self):
# 	#Not logged in
# 		#Success
# 		response = self.client.post('/lab-10/movie/watch_later/add/tt0457648/')
# 		self.assertEqual(response.status_code,302)

# 		#Movie is already in session
# 		response = self.client.post('/lab-10/movie/watch_later/add/tt0457648/')
# 		response = self.client.get('/lab-10/movie/detail/tt0457648/')
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Movie already exist on SESSION! Hacking detected!',html_response)


# 	def test_api_search_movie(self):
# 		#init search
# 		response = Client().get('/lab-10/api/movie/-/-/')
# 		self.assertEqual(response.status_code, 200)
# 		#search movie by title
# 		response = Client().get('/lab-10/api/movie/justice/-/')
# 		self.assertEqual(response.status_code, 200)
# 		#search movie by title and year
# 		response = Client().get('/lab-10/api/movie/justice/2016/')
# 		self.assertEqual(response.status_code, 200)
# 		# 0 > number of result <= 3
# 		response = Client().get('/lab-10/api/movie/Guardians of Galaxy/2016/')
# 		self.assertEqual(response.status_code, 200)
# 		#not found
# 		response = Client().get('/lab-10/api/movie/zabolaza/-/')
# 		self.assertEqual(response.status_code, 200)

# 	