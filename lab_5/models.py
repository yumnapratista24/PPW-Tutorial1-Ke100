from django.db import models

class Todo(models.Model):
    description = models.TextField()
    title = models.CharField(max_length=27)
    created_date = models.DateTimeField(auto_now_add=True)
